﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationContainer : MonoBehaviour
{
    public static ApplicationContainer Instance;

    public Sprite BackImage;

    public EventHolder EventHolder;

    private void Awake()
    {
        EventHolder = new EventHolder();
        if (Instance == null)
        {
            Instance = this;
        }
        
    }

    
}
