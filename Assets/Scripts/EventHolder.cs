﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHolder
{

    public Action<int> OnCardPressedEvent;
    public void OnCardPressed(int i)
    {
        OnCardPressedEvent(i);
    }

    public Action<bool> OnCardsBlockedEvent;
    public void OnCardsBlocked(bool block)
    {
        OnCardsBlockedEvent(block);
    }
    public Action<int> OnScoreChangedEvent;
    public void OnScoreChanged(int change)
    {
        OnScoreChangedEvent(change);
    }
}
