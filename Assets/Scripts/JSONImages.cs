﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JSONImages
{
    public string ver { get; set; }
    public List<string> strings { get; set; }
}

