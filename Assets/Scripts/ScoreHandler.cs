﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreHandler : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI m_textField;
    int score = 0;
    void Start()
    {
        m_textField.text = $"Score:{score}";
        ApplicationContainer.Instance.EventHolder.OnScoreChangedEvent += ChangeScore;
    }

    void ChangeScore(int change)
    {
        score += change;
        m_textField.text = $"Score:{score}";
    }
   
}
