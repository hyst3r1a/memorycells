﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUtility : MonoBehaviour
{
    public static IEnumerator Wait(int seconds, Action OnComplete)
    {
        yield return new WaitForSecondsRealtime(seconds);
        OnComplete.Invoke();
        
    }
}
