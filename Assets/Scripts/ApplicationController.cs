﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;

public class ApplicationController : MonoBehaviour
{

    [SerializeField] List<Card> m_imgList;
    [SerializeField] GridLayoutGroup m_grid;
   

    private List<Sprite> m_sprites;
    private List<int> m_currentSelections;
    
    public Texture myTexture;
    public string myJSON;
    JSONImages json;

    void Start()
    {
        m_grid.cellSize = new Vector2((Screen.width - m_grid.padding.left - m_grid.padding.right - m_grid.spacing.x * 2) / 3, (Screen.height - m_grid.padding.left - m_grid.padding.right - m_grid.spacing.y * 2) / 2);
        StartCoroutine(GetJSON());
        ApplicationContainer.Instance.EventHolder.OnCardPressedEvent += AddCard;
    }

    void AddCard(int i)
    {
        m_currentSelections.Add(i);
        if(m_currentSelections.Count >= 2)
        {
            CheckPair();
        }
    }

    void CheckPair()
    {
        ApplicationContainer.Instance.EventHolder.OnCardsBlocked(true);
        if (m_imgList[m_currentSelections[0]].GetImage().sprite == (m_imgList[m_currentSelections[1]].GetImage().sprite))
        {
            StartCoroutine(GameUtility.Wait(1, () =>
            {
                m_imgList[m_currentSelections[1]].GetImage().enabled = false;
                m_imgList[m_currentSelections[0]].GetImage().enabled = false;
                m_currentSelections.Clear();
                ApplicationContainer.Instance.EventHolder.OnCardsBlocked(false);
                ApplicationContainer.Instance.EventHolder.OnScoreChanged(1);
                CheckWin();
            }));
        }
        else
        {
            StartCoroutine(GameUtility.Wait(1, () =>
            {
                m_imgList[m_currentSelections[1]].Flip();
                m_imgList[m_currentSelections[0]].Flip();
                m_currentSelections.Clear();
                ApplicationContainer.Instance.EventHolder.OnCardsBlocked(false);
            }));
            
        }

        
    }

    void CheckWin()
    { 
        foreach(Card a in m_imgList)
        {
            if(a.GetImage().enabled == true)
            {
                return;
            }
        }
        
        SetupCards();
    }

    IEnumerator GetTexture()
    {
        m_sprites = new List<Sprite>();
        while (json.strings.Count > 0)
        {
            int j = Random.Range(0, json.strings.Count );
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(json.strings[j]);

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(json.strings[j]);
                myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                m_sprites.Add(SpriteFromTexture2D((Texture2D)myTexture));
            }
            json.strings.RemoveAt(j);
        }
        SetupCards();
    }

    void SetupCards() { 
       
        m_currentSelections = new List<int>();
        List<Image> m_currImgList = new List<Image>();
        List<Sprite> spritesCopy = new List<Sprite>();
        spritesCopy.AddRange(m_sprites);


        foreach (Card a in m_imgList)
        {
            m_currImgList.Add(a.GetImage());
        }

        
        while (m_currImgList.Count > 1)
        {
            int j = Random.Range(0, spritesCopy.Count);
            int i = Random.Range(0, m_currImgList.Count);

            m_currImgList[i].sprite = spritesCopy[j];
            m_currImgList[i].enabled = true;
            m_currImgList[i].GetComponent<Card>().SetSource(spritesCopy[j], i);
            m_currImgList.RemoveAt(i);


            int k = Random.Range(0, m_currImgList.Count);
            m_currImgList[k].sprite = spritesCopy[j];
            m_currImgList[k].enabled = true;
            m_currImgList[k].GetComponent<Card>().SetSource(spritesCopy[j], k);


            m_currImgList.RemoveAt(k);
            spritesCopy.RemoveAt(j);
        }
            
        
        StartCoroutine(GameUtility.Wait(5, () =>
        {
            foreach (Card card in m_imgList)
            {
                card.Flip();
            }
        }));
    }
    IEnumerator GetJSON()
    {

        UnityWebRequest www = UnityWebRequest.Get("https://drive.google.com/uc?export=download&id=1xBQi5WVr-wl6YYVf0bKUTMz-e_SVURHK");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            
             myJSON = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
            json = JsonConvert.DeserializeObject<JSONImages>(myJSON);   
        }
        yield return GetTexture();
    }
    Sprite SpriteFromTexture2D(Texture2D texture)
    {

        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
}