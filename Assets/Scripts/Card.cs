﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour
{
    [SerializeField] Image m_img;
    [SerializeField] Button m_button;
    [SerializeField] int m_key;

    private Sprite m_source;
    bool m_flipped = false;
    int m_number = -1;

    public void SetSource(Sprite newSource, int number)
    {
        m_source = newSource;
        m_img.sprite = newSource;
        m_button.interactable = false;
        m_number = number;
        ApplicationContainer.Instance.EventHolder.OnCardsBlockedEvent += Block;
    }
    public Image GetImage()
    {
        return m_img;
    }

    void Block(bool block)
    {
        if (block)
        {
            m_button.interactable = false;
        }
        else
        {
            m_button.interactable = true;
        }
    }

    int GetKey()
    {
        return m_key;
    }

    public void Flip()
    {
        if (m_flipped)
        {
            FlipBack();
           
        }
        else
        {
            FlipFront();
           
        }

    }

    private void FlipFront()
    {
        ApplicationContainer.Instance.EventHolder.OnCardsBlocked(true);
        m_flipped = true;
        transform.DOScale(new Vector3(0, 1, 1), 0.5f).SetEase(Ease.Linear).OnComplete(() =>
        {
            m_img.sprite = ApplicationContainer.Instance.BackImage;
            transform.DOScale(new Vector3(-1, 1, 1), 0.5f).SetEase(Ease.Linear).OnComplete(() =>
            {
                ApplicationContainer.Instance.EventHolder.OnCardsBlocked(false);
            });
            });
    }

    private void FlipBack()
    {
        ApplicationContainer.Instance.EventHolder.OnCardsBlocked(true);
        m_flipped = false;
        transform.DOScale(new Vector3(0, 1, 1), 0.5f).SetEase(Ease.Linear).OnComplete(() =>
        {
            m_img.sprite = m_source;
            transform.DOScale(new Vector3(1, 1, 1), 0.5f).SetEase(Ease.Linear).OnComplete(() =>
            {
                ApplicationContainer.Instance.EventHolder.OnCardPressed(m_key);
                ApplicationContainer.Instance.EventHolder.OnCardsBlocked(false);
            });
        });
    }

}
